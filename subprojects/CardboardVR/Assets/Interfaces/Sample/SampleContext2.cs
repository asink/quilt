﻿using interfaces;
using strange.extensions.command.impl;
using UnityEngine;

namespace implementations
{
	public class SampleContext2 : HelloWorldContext
	{
		public SampleContext2(MonoBehaviour contextView, bool autostart)
			: base(contextView, autostart)
		{
		}

		public SampleContext2(MonoBehaviour contextView, bool autostart, InterfaceMappingUtilities map)
			: base(contextView, autostart, map)
		{
		}

		protected override void mapBindings()
		{
			base.mapBindings();
			//injectionBinder.Bind<IWebService>().To<UnityWWWWebSerivce>();
			commandBinder.Bind<StartSignal>().To<LogClientInfoCommand>().To<TestCameraComponentsCommand>();
			//.To<LoadMainLevelCommand>();
		}
	}

	
}