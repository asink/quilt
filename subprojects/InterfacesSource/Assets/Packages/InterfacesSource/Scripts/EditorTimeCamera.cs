﻿using strange.extensions.context.impl;
using strange.extensions.implicitBind.api;
using strange.extensions.implicitBind.impl;
using strange.extensions.injector.api;
using strange.extensions.injector.impl;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace interfaces
{
	[ExecuteInEditMode]
	public class EditorTimeCamera : MonoBehaviour
	{
		private GameObject _currentCamera;
		[SerializeField] private CameraSettingsSO EditorBuildPreviewType;
		public HideFlags hideFlagsToUse = HideFlags.DontSave;

		public void Start()
		{
			if (Context.firstContext == null) return;
			enabled = false;
			Debug.Log("Shutting down camera");
		}

		[ContextMenu("testEnable")]
		public void OnEnable()
		{
			if (Application.isPlaying && Application.isEditor || Context.firstContext != null)
			{
				enabled = false;
				Debug.Log("Shutting down camera");
				return;
			}
			destroyCamera();

			//editor -- note: Returns true in the Unity editor when in play mode as well as Returns true when in any kind of player (Read Only).
			var vrCameraType = EditorBuildPreviewType;
			if (vrCameraType == null)
			{
				//TODO: make a context for this?
				//manually mocking up context work as a result of not doing that.. but making  it a context means that nested contexts would maybe cause issues?
				//ContextUtilities contextUtil = new ContextUtilities(_mappingUtilities.InterfaceMap, injectionBinder);
				IInjectionBinder injectionBinder = new InjectionBinder();
				ImplicitBinder implicitBinder = new ImplicitBinder
				{
					injectionBinder = injectionBinder,
					mediationBinder = new MediationBinder() //don't care about this..
				};
				implicitBinder.PostConstruct();

				implicitBinder.ScanForAnnotatedClasses(new[] {"interfaces", "implementations"});
				var contextUtil = new ContextUtilities(new InterfaceMappingUtilities().GetDefaultMap(), injectionBinder);
				contextUtil.standardInitialization();
				vrCameraType = injectionBinder.GetInstance<CameraSettingsSO>();
				//vrCameraType = Resources.Load<CameraSettingsSO>("NullVR");
			}
			_currentCamera = vrCameraType.Create();
			var CAMERA_OFFSET_POSITION = new Vector3(0f, 0f, 0f);
			_currentCamera.name = vrCameraType + " Editor Only(Do not edit)";
			_currentCamera.transform.parent = transform;
			_currentCamera.transform.localPosition = CAMERA_OFFSET_POSITION;

			this.SetHideflagsRecursive(hideFlagsToUse);
		}

		public void OnDisable()
		{
			destroyCamera();
		}

		[ContextMenu("Get sample camera settings")]
		public void PrintCameraSettings()
		{
			var camera = _currentCamera.GetComponentInChildren<Camera>();
			var settings = new SerializedCameraSettings
			{
				clearFlags = camera.clearFlags,
				depth = camera.depth,
				farClipPlane = camera.farClipPlane
				//fieldOfView = camera.fieldOfView
			};
			settings.SaveCameraBackgroundColor(camera.backgroundColor);
			Debug.Log(settings.JsonSerialize());
		}

		private void destroyCamera()
		{
			if (_currentCamera && _currentCamera != null)
			{
				var cameraToDestroy = _currentCamera;
				_currentCamera = null;
				if (cameraToDestroy.activeInHierarchy)
				{
					this.DestroyImmediateRecursive(cameraToDestroy);
					//if we're shutting down, don't worry about it.  otherwise, kill it
				}
			}
		}
	}
}

#if false
using strange.extensions.context.impl;
using UnityEngine;

namespace interfaces
{
	[ExecuteInEditMode]
	public class EditorTimeCamera : ContextView, IRoutineRunner
	{
		private InterfaceMappingUtilities utils = new InterfaceMappingUtilities();
		protected void Start()
		{
			if(Context.firstContext != null) return;
			context = new EditorTimeCameraContext(utils.GetDefaultMap());
			context.Start();
		}

		public void OnEnable()
		{
			if (Context.firstContext != null)
			{
				if (context != null && Context.firstContext != context)
				{
					enabled = false;
				}
			}
		}
	}

	public class EditorTimeCameraContext : SignalContext
	{
		private InterfaceMappingUtilities _mappingUtilities = new InterfaceMappingUtilities();

		public EditorTimeCameraContext(InterfaceMap getDefaultMap)
		{
			_mappingUtilities.InterfaceMap = getDefaultMap;
		}

		protected override void mapBindings()
		{
			base.mapBindings();

			ContextUtilities contextUtil = new ContextUtilities(_mappingUtilities.InterfaceMap, injectionBinder);

			contextUtil.standardInitialization();
		}
	}
}
#endif