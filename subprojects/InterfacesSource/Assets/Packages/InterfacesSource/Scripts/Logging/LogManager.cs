﻿using System.Collections.Generic;

namespace interfaces
{
	[Implements]
	public class LogManager : ILogger
	{
		[Inject]
		public List<ILogger> Loggers { get; set; }

		[PostConstruct]
		public void PostConstruct()
		{
			if(Loggers == null) {  Loggers = new List<ILogger>();}
		}

		public void Log(object toLog)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].Log(toLog);
			}
		}

		public void LogFormat(string format, params object[] parameters)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].LogFormat(format, parameters);
			}
		}

		public void LogWarning(object toLog)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].Log(toLog);
			}
		}

		public void LogWarningFormat(string format, params object[] parameters)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].LogWarningFormat(format, parameters);
			}
		}

		public void LogError(object toLog)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].Log(toLog);
			}
		}

		public void LogErrorFormat(string format, params object[] parameters)
		{
			for(int i = 0; i < Loggers.Count; i++)
			{
				Loggers[i].LogErrorFormat(format, parameters);
			}
		}
	}
}