﻿using System;

namespace UnityEditorCopies
{
	public enum iPhoneArchitecture_Copy
	{
		ARMv7 = 0,
		ARM64 = 1,
		Universal = 2
	}
	public enum RenderingPath_Copy
	{
		UsePlayerSettings = -1,
		VertexLit = 0,
		Forward = 1,
		DeferredLighting = 2,
		DeferredShading = 3,
	}
	[Flags]
	public enum BuildOptions_Copy
	{
		None = 0,
		Development = 1,
		AutoRunPlayer = 4,
		ShowBuiltPlayer = 8,
		BuildAdditionalStreamedScenes = 16,
		AcceptExternalModificationsToPlayer = 32,
		InstallInBuildFolder = 64,
		WebPlayerOfflineDeployment = 128,
		ConnectWithProfiler = 256,
		AllowDebugging = 512,
		SymlinkLibraries = 1024,
		UncompressedAssetBundle = 2048,
		[Obsolete("Use BuildOptions.Development instead")]
		StripDebugSymbols = 0,
		[Obsolete("Texture Compression is now always enabled")]
		CompressTextures = 0,
		ConnectToHost = 4096,
		DeployOnline = 8192,
		EnableHeadlessMode = 16384,
		BuildScriptsOnly = 32768,
		Il2CPP = 65536,
		ForceEnableAssertions = 131072,
	}
	
	
	
	public enum UIOrientation_Copy
	{
		Portrait,
		PortraitUpsideDown,
		LandscapeRight,
		LandscapeLeft,
		AutoRotation,
	}
	public enum ResolutionDialogSetting_Copy
	{
		Disabled,
		Enabled,
		HiddenByDefault,
	}
	public enum AspectRatio_Copy
	{
		AspectOthers,
		Aspect4by3,
		Aspect5by4,
		Aspect16by10,
		Aspect16by9,
	}
	public enum GraphicsDeviceType_Copy
	{
		OpenGL2 = 0,
		Direct3D9 = 1,
		Direct3D11 = 2,
		PlayStation3 = 3,
		Null = 4,
		Xbox360 = 6,
		OpenGLES2 = 8,
		OpenGLES3 = 11,
		PlayStationVita = 12,
		PlayStation4 = 13,
		XboxOne = 14,
		PlayStationMobile = 15,
		Metal = 16,
		OpenGLCore = 17,
		Direct3D12 = 18,
	}
	public enum ScriptingImplementation_Copy
	{
		Mono2x,
		IL2CPP,
		WinRTDotNET,
	}
	public enum ScriptCallOptimizationLevel_Copy
	{
		SlowAndSafe,
		FastButNoExceptions,
	}

	public enum iOSTargetDevice_Copy
	{
		iPhoneOnly,
		iPadOnly,
		iPhoneAndiPad,
	}
#region editorUserBuildSettings
	public enum BuildTarget_Copy
	{
		[Obsolete("Use BlackBerry instead (UnityUpgradable)", true)]
		BB10 = -1,
		[Obsolete("Use iOS instead (UnityUpgradable).", true)]
		iPhone = -1,
		StandaloneOSXUniversal = 2,
		StandaloneOSXIntel = 4,
		StandaloneWindows = 5,
		WebPlayer = 6,
		WebPlayerStreamed = 7,
		iOS = 9,
		PS3 = 10,
		XBOX360 = 11,
		Android = 13,
		StandaloneGLESEmu = 14,
		StandaloneLinux = 17,
		StandaloneWindows64 = 19,
		WebGL = 20,
		[Obsolete("Use WSAPlayer instead")]
		MetroPlayer = 21,
		WSAPlayer = 21,
		StandaloneLinux64 = 24,
		StandaloneLinuxUniversal = 25,
		WP8Player = 26,
		StandaloneOSXIntel64 = 27,
		BlackBerry = 28,
		Tizen = 29,
		PSP2 = 30,
		PS4 = 31,
		PSM = 32,
		XboxOne = 33,
		SamsungTV = 34,
	}
	public enum BuildTargetGroup_Copy
	{
		Unknown = 0,
		Standalone = 1,
		WebPlayer = 2,
		iOS = 4,
		[Obsolete("Use iOS instead (UnityUpgradable).", true)]
		iPhone = 4,
		PS3 = 5,
		XBOX360 = 6,
		Android = 7,
		GLESEmu = 9,
		WebGL = 13,
		[Obsolete("Use WSA instead")]
		Metro = 14,
		WSA = 14,
		WP8 = 15,
		BlackBerry = 16,
		Tizen = 17,
		PSP2 = 18,
		PS4 = 19,
		PSM = 20,
		XboxOne = 21,
		SamsungTV = 22,
	}
	public enum MobileTextureSubtarget_Copy
	{
		Generic,
		DXT,
		PVRTC,
		ATC,
		ETC,
		ETC2,
		ASTC,
	}
	public enum VRDeviceType_Copy
	{
		None,
		Stereo,
		Split,
		Oculus,
	}
#endregion
	

}