﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using UnityEditor.SceneManagement;

namespace interfaces
{	
	public class BuildConfigurationPickerWindow : EditorWindow {
		
		private class BuildConfigFile
		{
			public string Name;
			public BuildConfiguration Config;
			public BuildTarget BuildTarget
			{
				get
				{
					return SerializedPlayerSettingsEditor.getEnumFromCompatibleOne<BuildTarget>(Config.editorUserBuildSettings.currentBuildTarget.ToString());
				}
			}
			public MobileTextureSubtarget AndroidBuildSubtarget
			{
				get
				{
					return SerializedPlayerSettingsEditor.getEnumFromCompatibleOne<MobileTextureSubtarget>(Config.editorUserBuildSettings.subtarget.ToString());
				}
			}
			
			public BuildConfigFile(string name, BuildConfiguration config)
			{
				Name = name;
				Config = config;
			}
		}
		
		[MenuItem("quilt/Config Picker")]
		public static void Open()
		{
			//make sure that we mean to use the config picker in a subproject, since we haven't hammered out workflow for this
			if(ConfigBasedBuild.thisProjectIsASubproject())
			{
				Debug.Log("IS a subproject");
				if(EditorUtility.DisplayDialog("Error", "In a subproject, you likley do not want to use the config picker",
						"Oops, I didn't mean to do that", "I know what I'm doing, pleb"))
				{
					Debug.LogError("Not opening window");
					return;
				}
			}
			EditorWindow.GetWindow(typeof(BuildConfigurationPickerWindow), false, "Config Picker", true);
		}
		
		private string configDirPath;
        private BuildConfigFile selectedConfig = null;
        private BuildConfigFile[] configs;
		private Vector2 configListScrollPos;
        const string pythonRestartUnityProgram =
@"'import sys,os
from time import sleep
from subprocess import call
from shutil import rmtree
sleep(3)
rmtree(os.path.join(sys.argv[2],""Assets"",""Plugins""), ignore_errors=True)
call([sys.argv[1], ""-projectPath"", sys.argv[2], ""-executeMethod"", ""interfaces.CIRunner.ImportPackagesFromConfigBuild"", ""-BUILD_CONFIG_FILE_NAME="" + sys.argv[3]])'";
        const string pythonRestartUnityProgramWindows = @"import sys, os;from time import sleep;from subprocess import call;from shutil import rmtree;sleep(3);rmtree(os.path.join(sys.argv[2],'Assets','Plugins'), ignore_errors=True);call([sys.argv[1], '-projectPath', sys.argv[2], '-executeMethod', 'interfaces.CIRunner.ImportPackagesFromConfigBuild', '-BUILD_CONFIG_FILE_NAME=' + sys.argv[3]])";

        string ProjectPath {
			get {
				const string assetsDir = "Assets";
				return Application.dataPath.Substring(0, Application.dataPath.Length - assetsDir.Length);				
			}
		}
		
		private void OnFocus()
		{
			Refresh();
		}
		
		private void OnEnable()
		{
			Refresh();
		}
		
		private void OnGUI()
		{
			if(configDirPath == null)
			{
				Refresh();
			}
			
			//Config path readout and refresh
			GUILayout.BeginHorizontal();
			if(string.IsNullOrEmpty(configDirPath))
			{
				GUILayout.Label("Could not find configs directory", EditorStyles.miniLabel);
			}
			else
			{
				GUILayout.Label("Configs Directory:\n" + configDirPath, EditorStyles.miniLabel);
			}
			GUILayout.FlexibleSpace();
			if(GUILayout.Button("Refresh"))
			{
				Refresh();
			}
			GUILayout.EndHorizontal();
			
			//Sort configs into current / noncurrent platform
			List<BuildConfigFile> activePlatformConfigs = new List<BuildConfigFile>();
			List<BuildConfigFile> inactivePlatformConfigs = new List<BuildConfigFile>();
			foreach(BuildConfigFile config in configs)
			{
				bool isCurrentPlatform = EditorUserBuildSettings.activeBuildTarget == config.BuildTarget
					&& (config.BuildTarget != BuildTarget.Android || EditorUserBuildSettings.androidBuildSubtarget == config.AndroidBuildSubtarget);
				if(isCurrentPlatform)
				{
					activePlatformConfigs.Add(config);
				}
				else
				{
					inactivePlatformConfigs.Add(config);
				}
			}			
			
			//Active platform configs
			
			configListScrollPos = GUILayout.BeginScrollView(configListScrollPos);
			
			if(activePlatformConfigs.Count > 0)
			{
				GUILayout.Label("Configs for current platform:");
				foreach(BuildConfigFile config in activePlatformConfigs)
				{
					DrawBuildConfigFileListEntry(config, (x) => 
					{
                        selectedConfig = config;
					});
				}
			}
			//Inactive platform configs
			if(inactivePlatformConfigs.Count > 0)
			{
				GUILayout.Label("Configs for other platforms:");
				foreach(BuildConfigFile config in inactivePlatformConfigs)
				{
					DrawBuildConfigFileListEntry(config, (x) => 
					{
                        selectedConfig = config;
					});
				}
			}
			
			GUILayout.EndScrollView();
			
			//Delay this until end of GUI drawing to prevent stack issues
			if(selectedConfig != null)
			{
                CIRunner.DeletePackageFolders();
				SetPlatform(selectedConfig);				

				//Save and restart unity
				AssetDatabase.SaveAssets();
				EditorApplication.SaveAssets();
				AssetDatabase.Refresh();
				EditorApplication.delayCall += AskRestartUnity;
			}
		}
		
		private void DrawBuildConfigFileListEntry(BuildConfigFile config, System.Action<BuildConfigFile> onImport)
		{
			const int importButtonWidth = 56;
			
			GUILayout.BeginHorizontal();
			GUI.enabled = !EditorApplication.isUpdating && !EditorApplication.isCompiling;
			if(GUILayout.Button("Import", GUILayout.Width(importButtonWidth)))
			{
				onImport(config);
			}
			GUI.enabled = true;
			GUILayout.Label(config.Name);
			GUILayout.EndHorizontal();
		}
		
		private void Refresh()
		{
			try
			{
				configDirPath = ConfigBasedBuild.configsPath();
				configs = GetConfigFilesInDirectory(configDirPath);
			}
			catch
			{
				configDirPath = "";
				configs = new BuildConfigFile[0];
			}
		}
		
		private BuildConfigFile[] GetConfigFilesInDirectory(string dirPath)
		{
			DirectoryInfo dir = new DirectoryInfo(dirPath);
			FileInfo[] files = dir.GetFiles("*.txt");
			List<BuildConfigFile> configFiles = new List<BuildConfigFile>(files.Length);
			for (int i = 0; i < files.Length; i++)
			{
				try
				{
					configFiles.Add(new BuildConfigFile(files[i].Name, File.ReadAllText(files[i].FullName).JsonDeserialize<BuildConfiguration>()));
				}
				catch (System.Exception e)
				{
					Debug.LogWarning(files[i].Name + ": " + e.Message);
				}
			}
			return configFiles.ToArray();			
		}
		
		private void SetPlatform(BuildConfigFile config)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(config.BuildTarget);
			if(config.BuildTarget == BuildTarget.Android)
			{
				EditorUserBuildSettings.androidBuildSubtarget = config.AndroidBuildSubtarget;
			}
		}
		
		private void AskRestartUnity()
		{
			if(EditorUtility.DisplayDialog("Restart Unity", "Unity will now restart in order to unload native plugins.", "Ok", "Cancel"))
			{				
				if(!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
				{
					EditorUtility.DisplayDialog("Restart cancelled", "Some plugins will not be unloaded until Unity is restarted.", "Ok");
				}
				else
				{
					string unityPath = System.Environment.GetCommandLineArgs()[0];
					try
					{
                        if (Application.platform == RuntimePlatform.WindowsEditor)
                        {
                            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
                            {
                                FileName = "cmd",
                                Arguments = @"/c start """" /B ""python"" -c" + " \"" + pythonRestartUnityProgramWindows + "\" \"" + unityPath + "\" \"" + ProjectPath + "\" \"" + selectedConfig.Name + "\"",
                                CreateNoWindow = true,
                                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                            });
                        }
                        else
                        {
                            System.Diagnostics.Process.Start("python", "-c " + pythonRestartUnityProgram + " \"" + unityPath + "\" \"" + ProjectPath + "\" \"" + selectedConfig.Name + "\"");
                        }

						EditorApplication.Exit(0);
					}
					catch
					{
						Debug.LogError("Automatic restart failed: No python installation found");
						EditorUtility.DisplayDialog("Automatic restart failed", "Please close and restart unity.", "Ok");
					}
				}
			}
		}
	}	
}
