﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

#if NUNIT
using NUnit.Framework;
using UnityTest;
#endif

//TODO: pass in BUILD_NUMBER in System.Environment.GetCommandLineArgs

namespace interfaces
{
	public class CIRunner
	{
		private const string TARGET_DIR = "build"; //Application.dataPath + "/../build""/../build";


		//TODO: replace with closing and  calling gradle
		[MenuItem("quilt/extended/CI/Delete Package Folders")]
		public static void DeletePackageFolders()
		{
            string packagePath = Path.Combine("Assets", "Packages");
            if (!FileUtil.DeleteFileOrDirectory(packagePath))
            {
                Debug.LogWarning("Failed to delete " + packagePath);
            }

            string pluginsPath = Path.Combine("Assets", "Plugins");
            if (!FileUtil.DeleteFileOrDirectory(pluginsPath))
            {
                Debug.LogWarning("Failed to delete " + pluginsPath);
            }

            string streamingAssetsPath = Path.Combine("Assets", "StreamingAssets");
            if (!FileUtil.DeleteFileOrDirectory(streamingAssetsPath))
            {
                Debug.LogWarning("Failed to delete " + streamingAssetsPath);
            }

            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        }
		

		//waiting on EditorApplication.isUpdating and EditorApplication.isCompiling is the *correct* solution
		//the workable one is to split in 2 parts -- import configs, and then do build
		//[MenuItem("quilt/CI/config current based build")]
		public static void PerformConfigBuildForCurrentPlatform()
		{
			BuildFromFile(ConfigBasedBuild.CurrentPlatformConfigFile());
		}

		[MenuItem("quilt/extended/CI/config current based build")]
		public static void PerformConfigBuild()
		{
			var configFile = _cliHelper.GetStringFromCommandLine(CommandLineHelpers.ParameterNames.BUILD_CONFIG_FILE_NAME);
			PerformConfigBuild(configFile);
		}

		//NOTE: removed a ton of Refresh and saveassets calls keep an eye out if I end up missing assets!
		public static void PerformConfigBuild(string configFile)
		{
			ConfigBasedBuild.LoadConfigFile(configFile);
			/*
			 * var buildConfig = ConfigBasedBuild.LoadConfigFile(configFile);
			buildConfig.editorUserBuildSettings.SetStaticInstanceFromThis();
			 * 
			//this should be unnessesary
			buildConfig.buildConfig.SetStaticInstanceFromThis(buildConfig.editorUserBuildSettings.currentBuildTargetGroup);
			
			//ImportPackagesFromConfigBuildTryToReimport(buildConfig);
			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.SaveAssets();
			*/
			Debug.Log("Building with config file:" + configFile);
			BuildFromFile(configFile);
			/*
			Debug.Log("Done with config build");
			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.SaveAssets();
			*/
		}

		public static void BarTest()
		{
			Debug.Log("FOO TEST");
		}

		//[MenuItem("quilt/CI/foo test")]
		public static void FooTest()
		{
			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.SaveAssets();
			Debug.Log("FOO TEST");
		}
		public class SaveLastLogFileUsingConfigBasedBuildName
		{
			public static string lastConfigBuiltWith;
			[PostProcessBuild] // Requires Unity 3.5+
			private static void OnPostProcessBuildPlayer(BuildTarget target, string buildPath)
			{
				//the regular postbuildlog will wrtie platform/
				if(!string.IsNullOrEmpty(lastConfigBuiltWith))
					PostBuildLog.WriteBuildLog(buildPath, lastConfigBuiltWith.Replace(".txt", ""));
			}
		}

		public static void AndroidCLISettingsScan(bool debugPrint=true)
		{
			var toSet = new Dictionary<string, CommandLineHelpers.ParameterNames>()
			{
				{"AndroidSdkRoot", CommandLineHelpers.ParameterNames.ANDROID_SDK_HOME},
				{"AndroidNdkRoot", CommandLineHelpers.ParameterNames.ANDROID_NDK_HOME},
				{"JdkPath",        CommandLineHelpers.ParameterNames.JDK_HOME},
			};
			foreach(var toset in toSet)
			{
				try
				{
					var valueToSet = _cliHelper.GetStringFromCommandLine(toset.Value);
					if(!string.IsNullOrEmpty(valueToSet))
						EditorPrefs.SetString(toset.Key, valueToSet);
				}
				catch { if(debugPrint) Debug.Log("did not find/set " + toset.Key + " from command line option:" + toset.Value); }
			}
			try
			{
				var lastString = "";
				lastString = _cliHelper.GetStringFromCommandLine(CommandLineHelpers.ParameterNames.ANDROID_KEYSTORE);
				if(!string.IsNullOrEmpty(lastString))
					PlayerSettings.Android.keystoreName = lastString;
				lastString = _cliHelper.GetStringFromCommandLine(CommandLineHelpers.ParameterNames.ANDROID_KEYSTORE_ALIAS);
				//path_to_keystore
				if(!string.IsNullOrEmpty(lastString))
					PlayerSettings.Android.keyaliasName = lastString;
				lastString = _cliHelper.GetStringFromCommandLine(CommandLineHelpers.ParameterNames.ANDROID_KEYSTORE_AND_ALIAS_PASSWORD);
				//path_to_keystore
				if(!string.IsNullOrEmpty(lastString))
				{
					PlayerSettings.Android.keyaliasPass = lastString;
					PlayerSettings.keystorePass = lastString;
					PlayerSettings.Android.keystorePass = lastString;
				}

				if(debugPrint)  Debug.Log("set android keystore passwords, locations from first commandline then if not presetn, environment variable keystorename:" + PlayerSettings.Android.keystoreName);
			}
			catch(Exception e)
			{
				if(debugPrint) Debug.Log("Did not set android keystore passwords from command line+" + e);
			}
		}
		public static void BuildFromFile(string fileName)
		{
			var buildConfig = ConfigBasedBuild.LoadConfigFile(fileName);  //NOTE: loadconfigfile now also checks/sets build_number,build_type from command line
			SaveLastLogFileUsingConfigBasedBuildName.lastConfigBuiltWith = fileName;

			var cibuilder = new CIBuilder
			{
				buildTarget = EditorUserBuildSettings.activeBuildTarget,
				buildOptions = (BuildOptions) buildConfig.buildOptions,
				scenes = FindEnabledEditorScenes(),
				targetLocation = TARGET_DIR,
				targetName = buildConfig.app_output_name
			};

			Debug.Log(string.Format("{0} location {1} finalPath{2}", buildConfig.app_output_name, cibuilder.targetLocation,
				cibuilder.GetFinalPathName()));
			cibuilder.RunTests();
			cibuilder.Build();
		}
		
		[MenuItem("quilt/extended/CI/pc build")]
		private static void PerformPCConfigBuild()
		{
			PerformBuild(BuildTarget.StandaloneWindows);
		}

		[MenuItem("quilt/extended/CI/android build")]
		private static void PerformAndroidConfigBuild()
		{
			PerformBuild(BuildTarget.Android);
		}

#if UNITY_4_6
		public static void PerformBuild(BuildTarget target, AndroidBuildSubtarget subtarget = AndroidBuildSubtarget.ETC2)
		{
			BuildFromFile(ConfigBasedBuild.PlatformConfigFile(target, subtarget));
		}
#else
		public static void PerformBuild(BuildTarget target, MobileTextureSubtarget subtarget = MobileTextureSubtarget.ETC2)
		{
			BuildFromFile(ConfigBasedBuild.PlatformConfigFile(target, subtarget));
		}
#endif

		private static string[] FindEnabledEditorScenes()
		{
			var EditorScenes = new List<string>();
			foreach (var scene in EditorBuildSettings.scenes)
			{
				if (!scene.enabled) continue;
				EditorScenes.Add(scene.path);
			}
			return EditorScenes.ToArray();
		}

		private static readonly CommandLineHelpers _cliHelper = new CommandLineHelpers();

		
		private class CIBuilder
		{
			private readonly ILogger Log = new UnityLogger();
			public string[] scenes { get; set; }
			public string targetLocation { get; set; }
			public string targetName { get; set; }
			public BuildTarget buildTarget { get; set; }
			public BuildOptions buildOptions { get; set; }

			public void Build()
			{
				EditorUserBuildSettings.SwitchActiveBuildTarget(buildTarget);
				Log.Log("Active build: " + buildTarget + " to path : " + GetFinalPathName() + " Scenes: " + string.Join(",", scenes));

				var suffix = "";
				//some platforms require the target to be a foldername, others require a file with a specific extension
				switch (buildTarget)
				{
					case BuildTarget.StandaloneWindows:
					case BuildTarget.StandaloneWindows64:
						suffix = Path.DirectorySeparatorChar + targetName + ".exe";
						break;

					case BuildTarget.StandaloneOSXIntel:
					case BuildTarget.StandaloneOSXIntel64:
						suffix = Path.DirectorySeparatorChar + targetName + ".app";
						break;

					case BuildTarget.Android:
					case BuildTarget.Tizen:
					case BuildTarget.SamsungTV:
						suffix = Path.DirectorySeparatorChar + targetName + ".apk";
						break;

					case BuildTarget.iOS:
					case BuildTarget.WebPlayer:
						suffix = Path.DirectorySeparatorChar + targetName;
						break;
				}
				var targetToBuildFinal = GetFinalPathName() + suffix;
				Log.Log("path name passed into build player: " + targetToBuildFinal);
				var res = BuildPipeline.BuildPlayer(scenes, targetToBuildFinal, buildTarget, buildOptions);
				if (res.Length > 0)
					throw new Exception("BuildPlayer failure: " + res);

				Log.Log("Output of running build file : " + res);
			}

			public string GetFinalPathName()
			{
				//todo: string formatting
				var targetDir = targetLocation;
				//var targetDir = targetLocation + Path.DirectorySeparatorChar + buildTarget;
				targetDir = Path.GetFullPath(targetDir);

				//if (Directory.Exists(targetDir))
				//	Directory.Delete(targetDir, true); //AssetDatabase.DeleteAsset(targetDir); //Directory.Delete(targetDir, true);
				if(!Directory.Exists(targetDir))
					Directory.CreateDirectory(targetDir);
				/*
				var rawFullPathFromdataPath =
					string.Format(
						"{0}" + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "{1}" + Path.DirectorySeparatorChar +
						"{2}", Application.dataPath, targetLocation, buildTarget);
				*/
			var rawFullPathFromdataPath =
					string.Format("{0}" + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "{1}",
						Application.dataPath, targetLocation);
				var fullpath = Path.GetFullPath(rawFullPathFromdataPath);

				Log.Log("Final fullpath: " + fullpath);

				if (!Directory.Exists(fullpath))
					Directory.CreateDirectory(fullpath);

				return fullpath; //note this is because fb's api shits itself without the full path
			}

			public void RunTests()
			{
#if NUNIT
			UnitTestView.RunAllTestsBatch();
			//NunitTestRunner.RunAllTests(buildTarget.ToString());
			RunEditorTests.RunAllTests(buildTarget.ToString());
#endif //#if NUNIT
			}
		}
	}
}