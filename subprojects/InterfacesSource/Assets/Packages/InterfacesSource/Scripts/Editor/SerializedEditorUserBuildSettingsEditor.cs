﻿using System;
using UnityEditor;
using UnityEngine.VR;

namespace interfaces
{
	[Serializable]
	public class SerializedEditorUserBuildSettingsEditor : SerializedEditorUserBuildSettings
	{
#if false
		public bool allowDebugging;
		public BuildTarget currentBuildTarget;
		public BuildTargetGroup currentBuildTargetGroup;
		//assuming EditorUserBuildSettings.selectedBuildTargetGroup gets set when we switch groups

		//public bool appendProject;  //is now BiuldOptions.AcceptExternalModificationsToPlayer
		public bool development;
		public bool explicitNullChecks;
		public MobileTextureSubtarget subtarget; //was: activeBuildTarget
		public bool symlinkLibraries;
		public bool virtualRealitySupported;
#endif
		T getEnumFromCompatibleOne<T>(string input) //where T : Enum
		{
			//return (UnityEditor.UIOrientation)Enum.Parse(typeof(UnityEditor.UIOrientation), defaultInterfaceOrientation.ToString())
			return (T)Enum.Parse(typeof(T), input);
		}

		//[MenuItem("test/asinkthing")]
		public static void TestBuildTarget()
		{
			SerializedEditorUserBuildSettingsEditor foo = new SerializedEditorUserBuildSettingsEditor();
			//foo.getEnumFromCompatibleOne<BuildTarget>(currentBuildTarget.ToString());
			UnityEngine.Debug.Log(foo.getEnumFromCompatibleOne<BuildTarget>("StandaloneWindows"));
		}
		public void SetStaticInstanceFromThis()
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(getEnumFromCompatibleOne<BuildTarget>(currentBuildTarget.ToString()));
			EditorUserBuildSettings.androidBuildSubtarget = getEnumFromCompatibleOne<MobileTextureSubtarget>(subtarget.ToString());
			EditorUserBuildSettings.selectedBuildTargetGroup = getEnumFromCompatibleOne<BuildTargetGroup>(currentBuildTargetGroup.ToString());
			EditorUserBuildSettings.allowDebugging = allowDebugging;
			//EditorUserBuildSettings.
			EditorUserBuildSettings.development = development;
			EditorUserBuildSettings.explicitNullChecks = explicitNullChecks;

			EditorUserBuildSettings.symlinkLibraries = symlinkLibraries;
			VRSettings.enabled = vrEnabled;
			VRSettings.loadedDevice = getEnumFromCompatibleOne<VRDeviceType>(vrDeviceType.ToString());
		}

		public void GetFromStaticInstance()
		{
			allowDebugging = EditorUserBuildSettings.allowDebugging;
			subtarget = getEnumFromCompatibleOne<UnityEditorCopies.MobileTextureSubtarget_Copy>(EditorUserBuildSettings.androidBuildSubtarget.ToString());
			//NOTE: this will likely change, and not work for tizen,etc

			currentBuildTarget = getEnumFromCompatibleOne<UnityEditorCopies.BuildTarget_Copy>(EditorUserBuildSettings.activeBuildTarget.ToString());
			currentBuildTargetGroup = getEnumFromCompatibleOne<UnityEditorCopies.BuildTargetGroup_Copy>(EditorUserBuildSettings.selectedBuildTargetGroup.ToString());

			development = EditorUserBuildSettings.development;
			explicitNullChecks = EditorUserBuildSettings.explicitNullChecks;

			symlinkLibraries = EditorUserBuildSettings.symlinkLibraries;

			vrEnabled = VRSettings.enabled;
			vrDeviceType = getEnumFromCompatibleOne<UnityEditorCopies.VRDeviceType_Copy>(VRSettings.loadedDevice.ToString());
			//virtualRealitySupported = PlayerSettings.GetPropertyBool("virtualRealitySupported", currentBuildTargetGroup); //NOTE:this looks like what is heppening when you hit the player settings editor
		}
	}
}