﻿using strange.extensions.context.impl;
using UnityEngine;

namespace interfaces
{
	public class SampleContextView : ContextView
	{
		[SerializeField]
		protected InterfaceMappingUtilities utils;

		protected void Start()
		{
			utils.SetFrameRate();
			context = new SampleContext(this, true, utils);
			context.Start();
		}
	}
}