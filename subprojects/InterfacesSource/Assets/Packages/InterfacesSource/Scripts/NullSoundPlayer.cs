﻿using UnityEngine;
using System.Collections;

namespace interfaces
{
	//[Implements(typeof(ISoundPlayer))]
	public class NullSoundPlayer : ISoundPlayer
	{
		public float PlaySound(string soundName)
		{
			return 0f;
		}

		public float PlaySound(string soundName, Vector3 position)
		{
			return 0f;
		}

		public void StopSound(string soundName)
		{
		}

		public void StopGroup(SoundGroup @group)
		{
		}

		public void StopGroup(string @group)
		{
		}

		public void SetGlobalVolume(float weight)
		{
		}

		public float GetGlobalVolume()
		{
			return 0f;
		}

		public void SetGroupVolume(SoundGroup @group, float weight)
		{
		}

		public float GetGroupVolume(SoundGroup @group)
		{
			return 0f;
		}

		public IEnumerator Init()
		{
			yield return null;
		}

		public void Dispose()
		{
		}
	}
}