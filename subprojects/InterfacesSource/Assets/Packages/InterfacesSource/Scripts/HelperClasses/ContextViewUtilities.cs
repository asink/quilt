﻿using System;
using System.Collections.Generic;
using strange.extensions.injector.api;
using UnityEngine;
using Object = UnityEngine.Object;

namespace interfaces
{
	public enum NamedInjectionsCore
	{
		PRODUCT_NAME,
		BUILD_TYPE,
		BUILD_NUMBER,
		PLATFORM,
		LOGFILE_ENDPOINT
	}

	public class ContextUtilities
	{
		protected InterfaceMap _map;
		protected IInjectionBinder _injectionBinder;

		public ContextUtilities(InterfaceMap map, IInjectionBinder injectionBinder)
		{
			_map = map;
			_injectionBinder = injectionBinder;
		}

		public void standardInitialization()
		{
			var helper = new ReflectionHelpers();
			//map singletons
			foreach(var kvp in _map.interfaceMap)
			{
				//injectionBinder.Bind(helper.GetTypeInAssemblies(kvp.Key))
				//	.ToValue(Activator.CreateInstance(helper.GetTypeInAssemblies(kvp.Value))).ToSingleton();
				_injectionBinder.Bind(helper.GetTypeInAssemblies(kvp.Key)).To(helper.GetTypeInAssemblies(kvp.Value)).ToSingleton();
			}

			foreach(var namedStringType in _map.namedStringTypes)
			{
				_injectionBinder.Bind(namedStringType.Key).To(namedStringType.Value);
			}
			if(_injectionBinder.GetBinding<IVR>() != null && _map.interfaceMap.ContainsKey("IVR")) //NOTE: IVR binding is likely currently broken for other reasons -- TODO: test this with a config!
			{
				return;//other things should be crosscontext
				//return _injectionBinder.GetBinding<IVR>().value as IVR;
			}


			GetPlatform();
			SetupAnalytics(helper);

			var buildConfig = ResourceLoader.Load<BuildConfigSO>();
			_injectionBinder.Bind<BuildConfigSO>().ToValue(buildConfig).CrossContext();


			
			IVR vrCamera = mapCamera(helper);
			vrCamera.CreateCamera();

			_injectionBinder.Bind<IRoutineRunner>().ToValue(SingletonManager.instance.AddSingleton<RoutineRunner>()).CrossContext();

			var lifecycle = SingletonManager.instance.AddSingleton<MonobehaviourLifecycleMessages>();
			_injectionBinder.Bind<IMonobehaviourLifecycleMessages>().ToValue(lifecycle).CrossContext();

			//{"interfaceMap":{"IController":"UnityDefaultInputAxisController","IVR":"OculusDesktopVR"},"interfaceToMultipleMaps":{"IAnalytics":["CountlyAnalytics"]},"namedStringTypes":{},"namedIntTypes":{}}
			const string CameraSettingsJsonName = "CameraSettingsJson";
			var cameraSettingsJson = _map.namedStringTypes.ContainsKey(CameraSettingsJsonName)
				? _map.namedStringTypes[CameraSettingsJsonName]
				: null;
			LoadCamera(vrCamera, vrCamera.GetCameraSettingsSO(), cameraSettingsJson);


			SetupLoggers(helper);
			//NOTE: not setting StartSignal mapping, leaving that up to sub-contexts!
		}


		public void GetPlatform()
		{
			string platformString = Application.platform.ToString();
			platformString = platformString.Replace("WindowsEditor", "StandaloneWindows64");
			if(_map.namedStringTypes != null && _map.namedStringTypes.ContainsKey(NamedInjectionsCore.PLATFORM.ToString()))
			{
				platformString = _map.namedStringTypes[NamedInjectionsCore.PLATFORM.ToString()];
			}

			_injectionBinder.Bind<string>().ToName(NamedInjectionsCore.PLATFORM).ToValue(platformString).CrossContext();
			//Debug.Log("platform:" + NamedInjectionsCore.PLATFORM.ToString() + " and used key:" + platformString);
		}

		public void SetupAnalytics(ReflectionHelpers helper)
		{
			string analyticsTypeName = typeof(IAnalytics).Name;
			List<Type> analyticsSystems = new List<Type>();

			if(_map.interfaceToMultipleMaps.ContainsKey(analyticsTypeName))
			{
				var multiAnalytics = _map.interfaceToMultipleMaps[analyticsTypeName];

				foreach(var analyticsSystemName in multiAnalytics)
				{
					try
					{
						var system = helper.GetTypeInAssemblies(analyticsSystemName);
						if(system == null)
						{
							Debug.LogError("Didd not find desired analytics system:" + analyticsSystemName);
							continue;
						}
						analyticsSystems.Add(system);
					}
					catch(Exception e)
					{
						Debug.LogError("error setting up analytics system:" + analyticsSystemName + " error:" + e);
					}
				}
			}
			string initStringKey = InterfaceNamedInjections.ANALYTICS_INITIALIZATION.ToString();
			List<string> listOfInitParams = new List<string>();
			try
			{
				if(_map.namedStringTypes.ContainsKey(initStringKey))
				{
					string initString = _map.namedStringTypes[initStringKey];
					if(initString != null)
					{
						listOfInitParams = initString.JsonDeserialize<List<string>>();
					}
				}
			}
			catch(Exception e)
			{
				Debug.LogError("Error while decodnig analytics init:" + e); //fall back to null analytics here?
			}

			_injectionBinder.Bind<List<string>>()
							.ToValue(listOfInitParams)
							.ToName(InterfaceNamedInjections.ANALYTICS_INITIALIZATION).CrossContext();

			List<IAnalytics> analyticsSystemInstances = new List<IAnalytics>();

			foreach(var analyticsSystem in analyticsSystems)
			{
				//Debug.Log("Initializing analytics sytem:" + analyticsSystem.Name);
				analyticsSystemInstances.Add(Activator.CreateInstance(analyticsSystem) as IAnalytics);
			}

			_injectionBinder.Bind<List<IAnalytics>>().ToValue(analyticsSystemInstances).CrossContext();
			_injectionBinder.Bind<IAnalytics>().To<AnalyticsManager>().ToSingleton().CrossContext();
		}

		public void SetupLoggers(ReflectionHelpers helper)
		{
			List<ILogger> loggers = new List<ILogger>();
			string iloggerAsString = typeof(ILogger).ToString();
			if(_map.interfaceToMultipleMaps.ContainsKey(iloggerAsString))
			{
				foreach(string typeToTryToadd in _map.interfaceToMultipleMaps[iloggerAsString])
				{
					Type type = helper.GetTypeInAssemblies(typeToTryToadd);
					_injectionBinder.GetInstance(type);
				}
			}
			else
			{
				loggers.Add(_injectionBinder.GetInstance<RemoteLogger>());
				loggers.Add(_injectionBinder.GetInstance<UnityLogger>());
			}

			_injectionBinder.Bind<List<ILogger>>().ToValue(loggers).ToSingleton().CrossContext();
			_injectionBinder.Bind<ILogger>().To<LogManager>().ToSingleton().CrossContext();
		}

		public IVR mapCamera(ReflectionHelpers helper)
		{
			IVR vrCamera = null;
			if(_map.interfaceMap.ContainsKey("IVR"))
			{
				try
				{
					vrCamera = Activator.CreateInstance(helper.GetTypeInAssemblies(_map.interfaceMap["IVR"])) as IVR;
				}
				catch(Exception e)
				{
					//TODO: find a way to get the real logger!
					Debug.LogError("ERROR_INITIALIXING_IVR_FROM_MAP:" + e);
				}
			}
			if(vrCamera == null)
			{
				if(_injectionBinder.GetBinding<IVR>() != null)
				{
					vrCamera = _injectionBinder.GetInstance<IVR>();
				}
			}

			if(vrCamera == null)
				vrCamera = new NullVR();
			var ivrbinding = _injectionBinder.GetBinding<IVR>();
			if(ivrbinding == null)
			{
				_injectionBinder.Bind<IVR>().ToValue(vrCamera).ToSingleton().CrossContext();
			}
			return vrCamera;
		}
		public void LoadCamera(IVR vr, CameraSettingsSO cameraSettings, string cameraJsonSettings = null)
		{
			if(cameraJsonSettings == null)
			{
				cameraJsonSettings =
					@"{""clearFlags"":""Skybox"",""backgroundColor"":[0.13,0.15,0.2,0.01],""depth"":-1,""clearFlags"":""Skybox"",""textureResourcesName"":""Skybox/Skybox""}";
			}
			var cameraPrefab = Resources.Load<VRCameraFactory>("Camera");
			var cameraGOInstance = Object.Instantiate(cameraPrefab.gameObject) as GameObject;
			Object.DontDestroyOnLoad(cameraGOInstance);
			//make sure this doesn't end up in the scene perm
			//VRCameraFactory.SetHideflagsRecursive(cameraGOInstance, HideFlags.DontSave | HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild);

			var cameraFactory = cameraGOInstance.GetComponent<VRCameraFactory>();
			var cursorController = cameraGOInstance.GetComponent<CursorController>();
			var fadeCube = cameraGOInstance.GetComponentInChildren<FadeSphere>();

			cameraFactory.SetCameraSettingsSO(cameraSettings);

			_injectionBinder.Bind<CursorController>().ToValue(cursorController).CrossContext();
			_injectionBinder.Bind<FadeSphere>().ToValue(fadeCube).CrossContext();
			_injectionBinder.Bind<VRCameraFactory>().ToValue(cameraFactory).CrossContext();
			_injectionBinder.GetBinding<IVR>().ToSingleton(); //let's see if this fixes the IVR being imported as a factory...
			cameraFactory.enabled = true;
			var serializedCamera = cameraJsonSettings.JsonDeserialize<SerializedCameraSettings>();
			cameraFactory.SetCameraSettingsOnAllCameras(serializedCamera);
			if(serializedCamera.customCameraSettings != null)
				vr.SetCustomCameraSettings(serializedCamera.customCameraSettings);
			//cameraPrefab.enabled = true;
		}

	}
}
