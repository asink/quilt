﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof (RangedTooltipAttribute))]
public class RangedTooltipDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var range = attribute as RangedTooltipAttribute;
		var content = new GUIContent(label.text, range.text);
		switch (property.propertyType)
		{
			case SerializedPropertyType.Float:
				EditorGUI.Slider(position, property, range.min, range.max, content);
				break;
			case SerializedPropertyType.Integer:
				EditorGUI.IntSlider(position, property, (int) range.min, (int) range.max, content);
				break;
			default:
				EditorGUI.LabelField(position, label.text, "Use Range with float or int.");
				break;
		}
	}
}