{  
   "interfaceMap":{  
      "IVR":"NullVR"
   },
   "interfaceToMultipleMaps":{  
      "IAnalytics":[  
         "CountlyAnalytics",
         "GoogleAnalytics",
         "NullAnalytics"
      ]
   },
   "namedStringTypes":{  
      "ANALYTICS_INITIALIZATION":"{\"PRIMARY\":[\"fd635e67659c91b954fbb3dce72c9ed73a906c7c\"],\"SECONDARY\":[\"{\\\"androidTrackingCode\\\":\\\"UA-57906201-1\\\",\\\"IOSTrackingCode\\\":\\\"UA-57906201-2\\\",\\\"otherTrackingCode\\\":\\\"UA-57906201-3\\\",\\\"productName\\\":null,\\\"bundleIdentifier\\\":\\\"com.asink.interfaces\\\",\\\"bundleVersion\\\":null,\\\"dispatchPeriod\\\":5,\\\"sampleFrequency\\\":100,\\\"logLevel\\\":\\\"WARNING\\\",\\\"anonymizeIP\\\":false,\\\"productName\\\":\\\"interfaces\\\",\\\"bundleVersion\\\":\\\"1\\\",\\\"dryRun\\\":false}\"],\"TERTIARY\":[\"baz\"]}"
   },
   "namedIntTypes":{  

   }
}