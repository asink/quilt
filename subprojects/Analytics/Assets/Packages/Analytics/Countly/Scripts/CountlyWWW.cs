using System.Collections;
using System.Threading;
using UnityEngine;
using ThreadPriority = UnityEngine.ThreadPriority;

public class CountlyWWW : MonoBehaviour
{
	private static int _numberOfFailedTries;

	public static IEnumerator SendDataToServer()
	{
		// Skip out early if there's no internet connection
		var networkProblem = Application.internetReachability == NetworkReachability.NotReachable;
		if(networkProblem) _numberOfFailedTries++;

		string data;

		while (Countly.Instance.ConnectionQueue.Count > 0 && (!networkProblem))
		{
			data = Countly.Instance.ConnectionQueue.Peek();

			var response = Send(data, 0);
			yield return response;

			Countly.Log("returned -> " + response.text);

			if(response.error == null)
			{
				//Data is sent succesfully
				Countly.Instance.ConnectionQueue.Dequeue();
			} else
			{
				yield return new WaitForSeconds(Countly.Instance.SleepAfterFailedTry*_numberOfFailedTries);
				_numberOfFailedTries++;
				Countly.Log("ERROR -> " + response.error);
			}
		}

		if(Countly.SendDataToServer)
		{
			var waitingTime = Countly.Instance.DataCheckPeriod;
			if(networkProblem) waitingTime += Countly.Instance.SleepAfterFailedTry*_numberOfFailedTries;

			yield return new WaitForSeconds(waitingTime);
			CountlyUtil.RunCoroutine(SendDataToServer());
		}
	}

	public static WWW Send(string data, int sleepingTime)
	{
		Countly.Log("call -> " + data);
		var result = new WWW(Countly.Instance.ServerURL + "/i?" + data) {threadPriority = ThreadPriority.Low};
		Thread.Sleep(sleepingTime);
		return result;
	}
}