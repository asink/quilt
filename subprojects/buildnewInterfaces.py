#!/usr/bin/env python
import sys
import traceback
import argparse

import os
from os import listdir
from os.path import isfile, isdir, join

from time import sleep
from subprocess import call
from shutil import copytree
from distutils import dir_util


def unityCallMethod(method,project):
    options = "-quit -batchmode -nographics"
    executionString = "\"%s\" -projectPath \"%s\" %s -executeMethod %s" % (unity, project,options, method)

    print "exporting pacakge:"+project
    print executionString
    sys.stdout.flush()
    call(executionString, shell=True)
    sys.stdout.flush()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build the Interfaces and InterfacesSource packages.')
    parser.add_argument('--unity', required=True, help='The absolute path to the Unity application')

    args = parser.parse_args()

    #eg ."/Volumes/1tbhdd/Applications/Unity461f1/Unity.app/Contents/MacOS/Unity"
    unity = args.unity

    slash="/"
    if "nt" in os.name:
        print "on windows"
        slash="\\"

    print "ELEMENTS: (%s) " % (unity)

    packages = ["Analytics","AudioToolkitSoundPlayer","DiveVR","OculusVR","UnityCloudAnalytics"]

    path = os.path.realpath(__file__)
    path = os.path.dirname(path)

    InterfacesSourcePath=path+slash+"InterfacesSource"
    unityCallMethod("interfaces.BuildDLLConfigVOMenuOptions.BuildAllDLLconfigs",InterfacesSourcePath)
    sleep(1.0/5.0)
    unityCallMethod("interfaces.ExportSelectionAsPackage.ExportAsPackage",InterfacesSourcePath)

    targetInterfacesPath = slash.join([path,"Interfaces","Assets","Interfaces","InterfaceDlls"])
    sourceInterfaceDlls = slash.join([path,"InterfacesSource","Dlls"])
    print targetInterfacesPath + " ......." + sourceInterfaceDlls  + " ..... " + os.path.realpath(__file__)
    dir_util.copy_tree(sourceInterfaceDlls,targetInterfacesPath)
    #call("cp -rf InterfacesSource/Dlls/* Interfaces/Assets/Interfaces/InterfaceDlls", shell=True)
    unityCallMethod("interfaces.ExportSelectionAsPackage.ExportAsPackage",path+slash+"Interfaces")

    #overwrite all interfaces source editor scripts
    #call("replaceDllsInHere.sh",shell=True)
