﻿using System;
using System.Collections.Generic;
using interfaces;
using UnityEngine;

namespace implementations
{
	[Implements(typeof(IVR))]
	public class ViveVR : IVR
	{
		private readonly CameraLoader _loader;
		private Transform _cameraTransform;

		public ViveVR()
		{
			_loader = new CameraLoader("ViveVR");
		}

		public GameObject CameraRef { get; private set; }

		public GameObject CreateCamera()
		{
			return _loader.CreateCamera();
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
		}

		public void SetCustomCameraSettings(string settingString)
		{
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return new List<CameraCommands>();
		}

		public Vector3 Position(VRComponent pos)
		{
			return getComponent(pos).position;
		}

		public Quaternion Rotation(VRComponent pos)
		{
			return getComponent(pos).localRotation;
		}

		public Transform leftCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.LEFT_EYE).transform;
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.RIGHT_EYE).transform;
		}

		public Transform CameraTransform()
		{
			if (_loader.CameraRef == null)
			{
				return null;
			}

			return _cameraTransform ?? (_cameraTransform = _loader.CameraRef.transform);
		}

		private Transform getComponent(VRComponent component)
		{
			switch (component)
			{
				case VRComponent.LEFT_EYE:
				case VRComponent.RIGHT_EYE:
				case VRComponent.CENTER:
					return CameraTransform();
				default:
					throw new NotSupportedException("tried to get an unrecognized vr position:" + component);
			}
		}

		public Vector3 LocalPosition(VRComponent pos)
		{
			return getComponent(pos).localPosition;
		}
	}
}