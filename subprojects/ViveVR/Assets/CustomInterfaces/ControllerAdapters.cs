﻿using System;
using System.Collections;
using System.ComponentModel;
using interfaces;
using strange.extensions.signal.impl;
using UnityEngine;

namespace implementations
{
	public interface IControllerAdapter
	{
	}

	[Implements]
	public class ActionInitiatedSignal : Signal
	{
	}

	[Implements]
	public class TimedActionInitiatedSignal : Signal<float>
	{
	}
	[Serializable]
	[Implements]
	public class HandControllerModel
	{
		public enum HandControllerRelation
		{
			RIGHT,
			LEFT
			//POSITIONAL //potentially multiple ones, with falloff.  todo
		}

		public HandModel rightHand = new HandModel() { controllerType = HandControllerRelation.RIGHT };
		public HandModel leftHand = new HandModel() {controllerType = HandControllerRelation.LEFT};

		public class HandModel
		{
			[DefaultValue(HandControllerRelation.RIGHT)]
			public HandControllerRelation controllerType = HandControllerRelation.RIGHT;

			[Serializable]
			public class VibrateInfo
			{
				[DefaultValue(0.5f)]
				public float durationInSeconds = 0.5f;

				[DefaultValue(3)]
				public int numTimes = 3;
			}

			public Signal<VibrateInfo> doVibrate = new Signal<VibrateInfo>();
			public Signal<GameObject,Vector3,Quaternion> swapHandModel = new Signal<GameObject, Vector3, Quaternion>();
			public TransformInfo transfomInfo = new TransformInfo();
			public Signal OnTriggerDown = new Signal();
			public Signal<float> OnTriggerUp = new Signal<float>();

			public class TransformInfo
			{
				public Vector3 position;
				public Quaternion rotation;

				public void GetFrom(Transform transform)
				{
					position = transform.position;
					rotation = transform.rotation;
				}

				public void SetTransform(Transform otherTransform)
				{
					otherTransform.position = position;
					otherTransform.rotation = rotation;
				}
			}
		}

		
	}


#if CARDBOARD
	[Implements(typeof (IControllerAdapter))]
	public class NodControl : IControllerAdapter
	{
		[Inject]
		public ActionInitiatedSignal ActionInitiated { get; set; }

		[Inject]
		public IRoutineRunner runner { get; set; }

		[PostConstruct]
		public void PostConstruct()
		{
			runner.StartCoroutine(scanForPresses());
		}

		private IEnumerator scanForPresses()
		{
			while (true)
			{
				throw new NotImplementedException(
					"TODO: scan for changes in direction over a period of time with a minimum amount of degrees of movement");
				yield return null;
			}
		}
	}
#endif //CARDBOARD

	//#if GEARVR
	//[Implements(typeof(IControllerAdapter))]
	public class MouseControl : IControllerAdapter
	{
		[Inject]
		public ActionInitiatedSignal ActionInitiated { get; set; }
		[Inject]
		public TimedActionInitiatedSignal TimedAction { get; set; }
		[Inject]
		public IRoutineRunner runner { get; set; }

		[PostConstruct]
		public void PostConstruct()
		{
			runner.StartCoroutine(scanForPresses());
		}

		IEnumerator waitForMouseUpAndPropigate()
		{
			float timeDown = 0f;
			while(true)
			{
				yield return null;
				if(Input.GetMouseButtonDown(0))
				{
					Debug.LogError("Got mouse down before mouse up!");
					yield break;
				}

				if(Input.GetMouseButtonUp(0))
				{
					break;
				}
				timeDown += Time.deltaTime;
			}
			TimedAction.Dispatch(timeDown);
		}
		private IEnumerator scanForPresses()
		{
			while(true)
			{
				if(Input.GetMouseButtonDown(0))
				{
					ActionInitiated.Dispatch();
					runner.StartCoroutine(waitForMouseUpAndPropigate());
				}

				yield return null;
			}
		}
	}

	//#endif
}